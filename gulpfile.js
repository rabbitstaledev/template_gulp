var gulp        = require('gulp');
var fileinclude = require('gulp-file-include');
var sass        = require('gulp-sass');
var sourcemaps  = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var notify      = require('gulp-notify');
var plumber     = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var imagemin    = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');

//option
var runSequence = require('run-sequence');

//set paths
var paths = {
	templates: './partials',
	sass: 'scss'
};

//show all error in terminal
var displayError = function(error) {
  //Initial building up of the error
  var errorString = '[' + error.plugin.error.bold + ']';
  errorString += ' ' + error.message.replace("\n", "")//replace new line at the end

  //If the error contains the filename or line number add it to the string
  if(error.fileName) {
    errorString += ' in ' + error.fileName;
  }

  if(error.lineNumber) {
    errorString += ' on line ' + error.lineNumber.bold;
  }

  // This will output an error like the following:
  // [gulp-sass] error message in file_name on line 1
}

var onError = function(err) {
  notify.onError({
    title: 'SCSS',
    subtitle: 'Failure!',
    message: err
  })(err);
  // console.log("line" , err.line);
  // console.log("linenumber", err.lineName);
  console.log(err);
  this.emit('end');
}

gulp.task('fileinclude', function() {
	return gulp.src(['partials/*.html', '!partials/*.tmp.html'])
  	.pipe(fileinclude({
        prefix: '@@',
        basepath: '@file'
      }))
    .pipe(gulp.dest('./'))
    .pipe(browserSync.reload({ stream:true }))
});


gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir:  './'
    }
  })
});

// BrowserSync reload all Browsers
gulp.task('browsersync-reload', function () {
  browserSync.reload();
});

gulp.task('sass', function() {
  return gulp.src('assets/style/scss/main.scss')
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(plumber({errorHandler: onError}))
    .pipe(sass({style: 'expaned', outputStyle: 'compressed'}))
    .pipe(autoprefixer({browsers: ['last 2 versions'], cascade: false}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('assets/style/css'))
    .pipe(browserSync.reload({ stream:true }))
    .pipe(notify({ message: 'Styles task complete' }));
});

// Optimize images
gulp.task('images', function() {
    return gulp.src('assets/images/*')
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      use: [imageminPngquant()]
    }))
    .pipe(gulp.dest('assets/images'));
});

gulp.task('watch', ['browser-sync', 'sass'], function() {
  gulp.watch(['partials/**/*.html'], ['fileinclude'])
  gulp.watch(['assets/style/scss/*.scss'],['sass'])
  gulp.watch(['assets/**/*'], ['browsersync-reload'])
});

gulp.task('default',['fileinclude', 'sass', 'watch'], function(done) {
	console.log('gulp default');
  // runSequence()
});
